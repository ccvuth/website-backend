import { Component, h } from 'preact';
import xhr from 'xhr';

import { getTrans } from '../../services/language.service';
import * as style from './home.scss';

export default class Home extends Component {
  trans = getTrans();
  state = {
    isSubmitted: false
  };

  handleSubmit = event => {
    event.preventDefault();
    const form = new FormData(document.querySelector('#contact-form'));
    const body = {
      name: form.get('name'),
      email: form.get('email'),
      company: form.get('company'),
      phone: form.get('phone'),
      msg: form.get('msg')
    };
    xhr(
      {
        method: 'POST',
        body,
        json: true,
        uri: 'https://speeddevs.com/api/sendmail',
        headers: {
          'Content-Type': 'application/json'
        }
      },
      (err, resp, body) => {
        console.log(resp, body, err);
        this.setState({
          isSubmitted: true
        });
      }
    );
  };

  render() {
    return (
      <div class={style.home}>
        <section id="welcome" class={style.welcome}>
          <div className="app-limiter flex-col align-center">
            <h1>
              {this.trans.innoSolutionsTo} <br />
              <span>{this.trans.improveBusiness}</span>
            </h1>
          </div>
        </section>

        <section id="why" class={style.why}>
          <div className="app-limiter flex-col align-center">
            <h1>{this.trans.whyChooseUs}</h1>

            <div class={'flex-row ' + style['flex-row']}>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/icons/performance.svg" />
                <h2>{this.trans.performance}</h2>
                <p>{this.trans.performanceDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/icons/delivery.svg" />
                <h2>{this.trans.delivery}</h2>
                <p>{this.trans.deliveryDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/icons/secure.svg" />
                <h2>{this.trans.secure}</h2>
                <p>{this.trans.secureDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/icons/scalable.svg" />
                <h2>{this.trans.scalable}</h2>
                <p>{this.trans.scalableDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/icons/reliable.svg" />
                <h2>{this.trans.reliable}</h2>
                <p>{this.trans.reliableDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/icons/support.svg" />
                <h2>{this.trans.support}</h2>
                <p>{this.trans.supportDesc}</p>
              </div>
            </div>
          </div>
        </section>

        <section id="how" class={style.how}>
          <div className="app-limiter flex-col align-center">
            <h1>{this.trans.howItWorks}</h1>
            <div className="flex-row">
              <div class={'flex-row align-center ' + style.rowTimelineItem}>
                <div class={'flex-col ' + style.timelineItem}>
                  <img src="/assets/icons/discover.svg" alt="" />
                  <h2>{this.trans.discover}</h2>
                  <p>{this.trans.discoverDesc}</p>
                </div>
                <img src="/assets/icons/box-connection.svg" alt="" />
                <div class={'flex-col ' + style.timelineItem}>
                  <img src="/assets/icons/design.svg" alt="" />
                  <h2>{this.trans.design}</h2>
                  <p>{this.trans.designDesc}</p>
                </div>
                <img class={style.boxConnectionH} src="/assets/icons/box-connection.svg" alt="" />
                <img class={style.boxConnectionV} src="/assets/icons/box-connection-vertical.svg" alt="" />
                <div class={'flex-col ' + style.timelineItem}>
                  <img src="/assets/icons/develop.svg" alt="" />
                  <h2>{this.trans.develop}</h2>
                  <p>{this.trans.developDesc}</p>
                </div>
                <img src="/assets/icons/box-connection.svg" alt="" />
                <div class={'flex-col ' + style.timelineItem}>
                  <img src="/assets/icons/deliver.svg" alt="" />
                  <h2>{this.trans.deliver}</h2>
                  <p>{this.trans.deliverDesc}</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="services" class={style.ourServices}>
          <div className="app-limiter flex-col align-center">
            <h1>{this.trans.ourServices}</h1>

            <div class={'flex-row ' + style['flex-row']}>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/backgrounds/web.jpg" />
                <h2>{this.trans.web}</h2>
                <p>{this.trans.webDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/backgrounds/mobile.jpg" />
                <h2>{this.trans.mobile}</h2>
                <p>{this.trans.mobileDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/backgrounds/desktop.jpg" />
                <h2>{this.trans.desktop}</h2>
                <p>{this.trans.desktopDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/backgrounds/design.png" />
                <h2>{this.trans.appDesign}</h2>
                <p>{this.trans.appDesignDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/backgrounds/cloud.jpg" />
                <h2>{this.trans.cloudInfra}</h2>
                <p>{this.trans.cloudInfraDesc}</p>
              </div>
              <div class={style['flex-col'] + ' flex-col ' + style.icoTxt}>
                <img src="/assets/backgrounds/modernize-digitize.jpg" />
                <h2>{this.trans.rmd}</h2>
                <p>{this.trans.rmdDesc}</p>
              </div>
            </div>
          </div>
        </section>

        <section id="contact" class={style.contact}>
          <div className="app-limiter flex-col align-center">
            <h1>{this.trans.contactUs}</h1>
            {this.state.isSubmitted ? (
              <h2>{this.trans.thanks}</h2>
            ) : (
              <div className="flex-row">
                <form id="contact-form" className="flex-col" onSubmit={this.handleSubmit}>
                  <div className="flex-col">
                    <div className="flex-row">
                      <div className="input-container">
                        <input name="name" type="text" required placeholder=" " />
                        <label htmlFor="name">{this.trans.name}</label>
                      </div>

                      <div className="input-container">
                        <input name="email" required type="email" placeholder=" " />
                        <label htmlFor="email">{this.trans.email}</label>
                      </div>
                    </div>

                    <div className="flex-row">
                      <div className="input-container">
                        <input name="company" type="text" placeholder=" " />
                        <label htmlFor="company">{this.trans.company}</label>
                      </div>
                      <div className="input-container">
                        <input name="phone" type="tel" required placeholder=" " />
                        <label htmlFor="phone">{this.trans.phone}</label>
                      </div>
                    </div>

                    <div className="input-container">
                      <input name="msg" type="text" required placeholder=" " />
                      <label htmlFor="msg">{this.trans.message}</label>
                    </div>
                  </div>

                  <input type="submit" value={this.trans.sendMessage} />
                </form>
                {/* <div>Address</div> */}
              </div>
            )}
          </div>
        </section>
      </div>
    );
  }
}
