import SingletonService from "./singleton.service";

class LanguageService extends SingletonService {}

function getTrans() {
    return LanguageService.getInstance().data.lang;
}

export { LanguageService, getTrans };
