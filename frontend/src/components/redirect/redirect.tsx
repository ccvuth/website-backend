import { Component } from "preact";
import { route } from "preact-router";

interface Props {
    to: string;
}

export default class Redirect extends Component<Props> {
    componentDidMount() {
        route(this.props.to, true);
    }

    render() {
        return null;
    }
}
