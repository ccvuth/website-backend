import { FunctionalComponent, h } from 'preact';
import { Link } from 'preact-router/match';

import { getTrans } from '../../services/language.service';
import style from './header.scss';

interface Props {
  topOffset?: number;
}

const Header: FunctionalComponent<Props> = (props: Props) => {
  const trans = getTrans();

  return (
    <header class={props?.topOffset > 20 ? style.nonActive : ''}>
      <div className="flex-row align-center app-limiter">
        <img class={style.logo} src="/assets/icons/logo.svg" alt="" />
        <span>Speed Devs</span>

        <div class={style.contacts + ' flex-row'}>
          <span className="align-center">
            <img src="/assets/icons/mail.svg" alt="" />
            dev.vision@speeddevs.com
          </span>
          <span className="align-center">
            <img src="/assets/icons/phone.svg" alt="" />
            +855 98 293451
          </span>
        </div>
      </div>
    </header>
  );
};

export default Header;
