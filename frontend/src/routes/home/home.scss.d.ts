// This file is automatically generated from your CSS. Any edits will be overwritten.
declare namespace HomeScssNamespace {
  export interface IHomeScss {
    arrowLd: string;
    boxConnectionH: string;
    boxConnectionV: string;
    contact: string;
    'flex-col': string;
    'flex-row': string;
    flipX: string;
    home: string;
    how: string;
    icoTxt: string;
    ourServices: string;
    rowTimelineItem: string;
    timelineItem: string;
    welcome: string;
    why: string;
  }
}

declare const HomeScssModule: HomeScssNamespace.IHomeScss;

export = HomeScssModule;
