'use strict';
var express = require('express');
var router = express.Router();
const nodemailer = require('nodemailer');

const email = process.env.MAIL_USR;
const pwd = process.env.MAIL_PWD;

let transporter = nodemailer.createTransport({
	host: 'mail.privateemail.com',
	port: 465,
	auth: {
		user: email,
		pass: pwd
	}
});

transporter.verify(function (error, success) {
	if (error) {
		console.log(error);
	} else {
		console.log('Server is ready to take our messages');
	}
});

router.post('/', function (req, res, next) {

	var sendmailreq = "<html>\n\
                        <body>\n\
                        <p> Name: " + req.body.name + "</p>\n\
                        <p> Email: " + req.body.email + "</p>\n\
                        <p> Company: " + req.body.company + "</p>\n\
                        <p> Phone: " + req.body.phone + "</p>\n\
                        <p> Message: " + req.body.msg + "</p>\n\
                        </body>\n\
                    </html>";
	var sendmailrevemail = "<html>\n\
														<body>\n\
															<h1>Thank you for your enquiry</h1>\n\
															<br/>\n\
															<h2>We will respond to you as soon as possible</h2>\n\
															<br/>\n\
															<h3>From Speed Devs Team<h3/>\n\
														</body>\n\
													</html>";

	var sender_email = req.body.email;

	let mailOptions = {
		from: email,
		to: email,
		subject: "Query from " + req.body.email + "",
		html: sendmailreq
	};

	let mailreverse = {
		from: email,
		to: sender_email,
		subject: "Your query on speeddevs.com",
		html: sendmailrevemail
	};
	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			return console.log(error);
		}
		console.log('Message %s sent: %s', info.messageId, info.response);
	});


	transporter.sendMail(mailreverse, (error, info) => {
		if (error) {
			return console.log(error);
		}
		console.log('Message %s sent: %s', info.messageId, info.response);
	});
	res.send("email send")

});

module.exports = router;
