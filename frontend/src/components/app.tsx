import { Component, h } from 'preact';
import { Route, Router } from 'preact-router';

import en from '../i18n/en.json';
import kh from '../i18n/kh.json';
import Home from '../routes/home/home';
import { LanguageService } from '../services/language.service';
import Header from './header/header';
import Redirect from './redirect/redirect';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
if ((module as any).hot) {
  // tslint:disable-next-line:no-var-requires
  require('preact/debug');
}

type Lang = 'en' | 'kh';

export default class App extends Component {
  state = {
    topOffset: 0
  };

  lang: any = location.pathname.slice(1, 3);
  instance = LanguageService.getInstance();
  temp = (this.instance.data.lang = this.assignLang(this.lang));

  assignLang(lang: Lang) {
    switch (lang) {
      case 'kh':
        return kh;
      default:
        return en;
    }
  }

  render() {
    return (
      <main
        id="app"
        class="flex-col"
        onScroll={(event: any) =>
          this.setState({
            topOffset: event.target.scrollTop
          })
        }
      >
        <Header topOffset={this.state.topOffset} />
        <Router>
          <Route path="/" component={Home} />
          <Route path="/kh" component={Home} />
          <Redirect to="/" default />
        </Router>
      </main>
    );
  }
}
